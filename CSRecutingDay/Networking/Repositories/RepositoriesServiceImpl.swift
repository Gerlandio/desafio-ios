//
//  RepositoriesServiceImpl.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 30/10/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit
import Moya

typealias RepositoriesCallbackFunction = (Result<RepositoriesResult>) -> Void

class RepositoriesServiceImpl: NSObject {

    func fetchRepositories(language: String, page: Int = 0, callback: @escaping RepositoriesCallbackFunction) {
        let repositoriesProvider = MoyaProvider<RepositoriesService>()
        
        repositoriesProvider.request(.fetchRepositories(language: language, page: page)) { (result) in
            switch result {
            case .success(let successResult):
                do {
                    let jsonDecoder = JSONDecoder()
                    let repositoriesResponse = try jsonDecoder.decode(RepositoriesResult.self, from: successResult.data)
                    callback(.success(repositoriesResponse))
                } catch let error {
                    callback(.error(error.localizedDescription))
                }
            case .failure(let error):
                callback(.error(error.localizedDescription))
            }
        }
    }
}
