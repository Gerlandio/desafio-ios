//
//  RepositoriesServiceImpl.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 30/10/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit
import Moya

typealias PullRequestsCallbackFunction = (Result<[PullRequest]>) -> Void

class PullRequestsServiceImpl: NSObject {

    func fetchPullRequests(fullName: String, callback: @escaping PullRequestsCallbackFunction) {
        let repositoriesProvider = MoyaProvider<RepositoriesService>()
        
        repositoriesProvider.request(.fetchPullRequests(fullName: fullName)) { (result) in
            switch result {
            case .success(let successResult):
                do {
                    let jsonDecoder = JSONDecoder()
                    let pullRquestResponse = try jsonDecoder.decode([PullRequest].self, from: successResult.data)
                    callback(.success(pullRquestResponse))
                } catch let error {
                    callback(.error(error.localizedDescription))
                }
            case .failure(let error):
                callback(.error(error.localizedDescription))
            }
        }
    }
}
