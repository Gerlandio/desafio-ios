//
//  RepositoryService.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 29/10/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit
import Moya

enum Result<T> {
    case success(T)
    case error(String)
}

enum RepositoriesService {
    case fetchRepositories(language: String, page: Int)
    case fetchPullRequests(fullName: String)
}

extension RepositoriesService: TargetType {
    
    var baseURL: URL{ return URL(string:"https://api.github.com/")! }
    
    var method: Moya.Method {
        switch self {
        case .fetchRepositories(_, _), .fetchPullRequests(_):
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .fetchRepositories(_, _):
            return "search/repositories"
        case .fetchPullRequests(let fullName):
            return "repos/\(fullName)/pulls"
        }
    }
    
    var task: Task {
        switch self {
        case .fetchRepositories(let language, let page):
            return .requestParameters(parameters: ["q": "language:\(language)", "sort": "stars", "page": page], encoding: URLEncoding.queryString)
        case .fetchPullRequests(_):
            return .requestParameters(parameters: [:], encoding: URLEncoding.queryString)
        }
    }
        
    var headers: [String : String]? {
        return ["Content-type": "application/json",
                "Accept"      : "application/json"]
    }
    
    var sampleData: Data {
        switch self {
        case .fetchRepositories(_, _), .fetchPullRequests(_):
            return "{\"total_count\": 325891,\"incomplete_results\": false,\"items\": [{\"id\": 22458259,\"name\": \"Alamofire\",\"full_name\": \"Alamofire/Alamofire\",\"owner\": {\"login\": \"Alamofire\",\"id\": 7774181,\"avatar_url\": \"https://avatars3.githubusercontent.com/u/7774181?v=4\",\"url\": \"https://api.github.com/users/Alamofire\",},\"description\": \"Elegant HTTP Networking in Swift\",\"url\": \"https://api.github.com/repos/Alamofire/Alamofire\",\"forks_url\": \"https://api.github.com/repos/Alamofire/Alamofire/forks\",\"keys_url\": \"https://api.github.com/repos/Alamofire/Alamofire/keys{/key_id}\",\"branches_url\": \"https://api.github.com/repos/Alamofire/Alamofire/branches{/branch}\",\"contributors_url\": \"https://api.github.com/repos/Alamofire/Alamofire/contributors\",\"issues_url\": \"https://api.github.com/repos/Alamofire/Alamofire/issues{/number}\",\"pulls_url\": \"https://api.github.com/repos/Alamofire/Alamofire/pulls{/number}\",\"milestones_url\": \"https://api.github.com/repos/Alamofire/Alamofire/milestones{/number}\",\"releases_url\": \"https://api.github.com/repos/Alamofire/Alamofire/releases{/id}\",\"created_at\": \"2014-07-31T05:56:19Z\",\"updated_at\": \"2017-10-29T17:24:13Z\",\"pushed_at\": \"2017-10-27T11:18:44Z\",\"git_url\": \"git://github.com/Alamofire/Alamofire.git\",\"ssh_url\": \"git@github.com:Alamofire/Alamofire.git\",\"clone_url\": \"https://github.com/Alamofire/Alamofire.git\",\"svn_url\": \"https://github.com/Alamofire/Alamofire\",\"size\": 4337,\"stargazers_count\": 25674,\"watchers_count\": 25674,\"language\": \"Swift\",\"forks_count\": 4457,\"open_issues_count\": 46,\"forks\": 4457,\"default_branch\": \"master\",\"score\": 1}]}".data(using: .utf8) ?? Data()
        }
    }
}
