//
//  PullRequestPresenter.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 03/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit

struct PullRequestPresenter {

    static func showPullRequestViewController(repository: Repository, from: UIViewController) {
        if let pullRequestController =  R.storyboard.repositorieDetails.instantiateInitialViewController() {
            pullRequestController.currentRepositorie = repository
            from.show(pullRequestController, sender: nil)
        }
    }
    
    static func showWebViewController(urlPullRequest: URL, from: UIViewController) {
        if let webviewController =  R.storyboard.repositorieDetails.webViewController() {
            webviewController.url = urlPullRequest
            from.show(webviewController, sender: nil)
        }
    }
}
