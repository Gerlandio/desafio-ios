//
//  Items.swift
//
//  Created by Gerlandio Da Silva Lucena on 30/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Repository: Codable {

    var forksUrl: String?
    var forks: Int?
    var size: Int?
    var pushedAt: String?
    var id: Int?
    var owner: Owner?
    var url: String?
    var name: String?
    var updatedAt: String?
    var score: Int?
    var fullName: String?
    var descriptionValue: String?
    var stargazersCount: Int?
    var createdAt: String?
    var openIssuesCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case forksUrl = "forks_url"
        case size
        case forks
        case pushedAt = "pushed_at"
        case id
        case owner
        case url
        case name
        case updatedAt = "updated_at"
        case score
        case fullName = "full_name"
        case descriptionValue = "description"
        case stargazersCount = "stargazers_count"
        case createdAt = "created_at"
        case openIssuesCount = "open_issues_count"
    }

}
