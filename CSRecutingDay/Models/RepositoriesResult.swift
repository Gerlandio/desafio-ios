//
//  BaseClass.swift
//
//  Created by Gerlandio Da Silva Lucena on 30/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation

struct RepositoriesResult: Codable {

    var incompleteResults: Bool? = false
    var totalCount: Int?
    var items: [Repository]?
    
    enum CodingKeys: String, CodingKey{
        case items
        case incompleteResults = "incomplete_results"
        case totalCount = "total_count"
    }
}
