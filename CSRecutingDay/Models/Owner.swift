//
//  Owner.swift
//
//  Created by Gerlandio Da Silva Lucena on 30/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Owner: Codable {

    var login: String?
    var avatarUrl: String?
    var id: Int?
    var url: String?

    enum CodingKeys: String, CodingKey{
        case login
        case avatarUrl = "avatar_url"
        case id
        case url
    }
}
