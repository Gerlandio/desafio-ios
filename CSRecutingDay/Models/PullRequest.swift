//
//  PullRequest.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 03/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit

struct PullRequest: Codable {
    var state: String?
    var body: String?
    var locked: Bool? = false
    var id: Int?
    var reviewCommentUrl: String?
    var title: String?
    var commentsUrl: String?
    var url: String?
    var issueUrl: String?
    var user: User?
    var updatedAt: String?
    var number: Int?
    var htmlUrl: String?
    var commitsUrl: String?
    var createdAt: String?

    enum CodingKeys: String, CodingKey {
        case state
        case body
        case locked
        case id
        case htmlUrl = "html_url"
        case reviewCommentUrl = "review_comment_url"
        case title
        case commentsUrl = "comments_url"
        case url
        case issueUrl = "issue_url"
        case user
        case updatedAt = "updated_at"
        case number = "number"
        case commitsUrl = "commits_url"
        case createdAt = "created_at"
    }
}
