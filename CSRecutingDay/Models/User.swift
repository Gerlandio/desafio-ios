//
//  User.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 03/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit

struct User: Codable {
    public var avatarUrl: String?
    public var login: String?
    public var id: Int?
    public var url: String?

    enum CodingKeys: String, CodingKey {
        case avatarUrl = "avatar_url"
        case login
        case id
        case url
    }
}
