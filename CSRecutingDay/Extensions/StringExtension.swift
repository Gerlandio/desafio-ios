//
//  StringExtension.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 03/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit

extension String {
    
    func formatDateDefault() -> String? {
        return formatDate(fromFormat: "yyyy-MM-dd'T'HH:mm:ss'Z'", toFormat: "dd/MM/yyyy 'às' HH:mm:ss")
    }
    
    func formatDate(fromFormat: String, toFormat: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        guard let date = dateFormatter.date(from: self) else {
            return nil
        }
        
        dateFormatter.dateFormat = toFormat
        return dateFormatter.string(from: date)
    }
    
    func registerNotification(listener: Any, callback: Selector) {
        NotificationCenter.default.addObserver(listener, selector: callback, name: Notification.Name(self), object: nil)
    }
    
    func postNotification(object: Any) {
        NotificationCenter.default.post(name: Notification.Name(self), object: object)
    }
}
