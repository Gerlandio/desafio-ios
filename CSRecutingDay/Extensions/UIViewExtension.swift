//
//  UIViewExtension.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 04/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit

class UIViewExtension: UIView {
    class func loadNib<view: UIView>() -> view? {
        return Bundle.main.loadNibNamed(String(describing: view.self), owner: nil, options: nil)?.first as? view
    }
}
