//
//  ColorExtension.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 01/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var defaultNavigationTintColor: UIColor {
        get {
            return UIColor.white
        }
    }
    
    static var defaultNavigationBarTintColor: UIColor {
        get {
            return UIColor.black
        }
    }
    
    static var defaultNavigationTitleAttribute: [NSAttributedStringKey : Any] {
        get {
            return [NSAttributedStringKey.foregroundColor:UIColor.white]
        }
    }
    
    static var defaultOrangeColor: UIColor {
        get {
            return UIColor(red: 241.0/255.0, green: 106.0/255.0, blue: 53.0/255.0, alpha: 1)
        }
    }
    
}
