//
//  SimpleInformationDataSource.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 03/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit

class SimpleInformationDataSource: BaseItemDataSource<String> {
    
    override var identifier: String{ get { return String(describing: SimpleTextTableViewCell.self) } }
    
    override func setupTableView() {
        self.tableView?.register(cellType: SimpleTextTableViewCell.self)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        if let cellPullRequest = cell as? SimpleTextTableViewCell {
            let pullRequest = itemList?[indexPath.row]
            cellPullRequest.item = pullRequest
        }
        return cell
    }
}
