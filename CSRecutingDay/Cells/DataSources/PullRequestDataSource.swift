//
//  RepositoriesDataSource.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 01/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit
import Reusable

class PullRequestDataSource: BaseItemDataSource<PullRequest> {
    
    override var identifier: String{ get { return String(describing: PullRequestTableViewCell.self) } }
    
    override func setupTableView() {
        self.tableView?.register(cellType: PullRequestTableViewCell.self)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        if let cellPullRequest = cell as? PullRequestTableViewCell {
            let pullRequest = itemList?[indexPath.row]
            cellPullRequest.item = pullRequest
        }
        return cell
    }
}
