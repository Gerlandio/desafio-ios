//
//  UserInformationView.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 01/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit
import SDWebImage

class UserInformationView: UIView {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userFullNameLabel: UILabel!
    @IBOutlet weak var userAvatarImage: UIImageView!
    
    func setupView(userName: String?, userFullName: String?, avatarUrl: String?) {
        self.userNameLabel.text = userName
        self.userFullNameLabel.text = userFullName
        if let avatar = avatarUrl, let urlAvatar = URL(string: avatar) {
            self.userAvatarImage.sd_setImage(with: urlAvatar, placeholderImage: R.image.avatar())
        }
    }
}
