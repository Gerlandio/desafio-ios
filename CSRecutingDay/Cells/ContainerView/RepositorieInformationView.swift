//
//  RepositorieInformationView.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 01/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit

class RepositorieInformationView: UIView {
    @IBOutlet weak var repoNameLabel: UILabel!
    @IBOutlet weak var repoDescriptionLabel: UILabel!
    
    func setupView(repositorieName: String?,descriptionValue: String?) {
        self.repoNameLabel.text = repositorieName
        self.repoDescriptionLabel.text = descriptionValue
    }
}
