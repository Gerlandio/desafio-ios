//
//  RepositorieTableViewCell.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 01/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit
import Reusable

class BaseInformationTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var userInformationView: UserInformationView?
    @IBOutlet weak var repositorieView: RepositorieInformationView?
    
    func setUserInformation(userName: String?, userFullName: String?, avatarUrl: String?) {
        self.userInformationView?.setupView(userName: userName, userFullName: userFullName, avatarUrl: avatarUrl)
    }
    
    func setRepositoryInformatio(title: String?, detail: String?) {
        self.repositorieView?.setupView(repositorieName: title,descriptionValue: detail)
    }
}
