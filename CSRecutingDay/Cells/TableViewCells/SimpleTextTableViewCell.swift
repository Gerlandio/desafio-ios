//
//  SimpleTextTableViewCell.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 03/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit
import Reusable

class SimpleTextTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var textInformation: UILabel!

    var item: String? {
        didSet {
            setupCell()
        }
    }
    
    func setupCell() {
        textInformation.text = item
    }
}
