//
//  RepositorieTableViewCell.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 01/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit

class RepositorieTableViewCell: BaseInformationTableViewCell {
    
    @IBOutlet weak var pullRequestLabel: UILabel?
    @IBOutlet weak var startCountLabel: UILabel?
    
    var item: Repository? {
        didSet{
            setupView()
        }
    }
    
    func setupView() {
        setUserInformation(userName: item?.owner?.login, userFullName: item?.fullName, avatarUrl: item?.owner?.avatarUrl)
        
        setRepositoryInformatio(title: item?.name, detail: item?.descriptionValue)
        
        self.startCountLabel?.text = "\(item?.stargazersCount ?? 0)"
        self.pullRequestLabel?.text = "\(item?.forks ?? 0)"
    }
}
