//
//  PullRequestTableViewCell.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 03/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: BaseInformationTableViewCell {
    
    var item: PullRequest? {
        didSet{
            setupView()
        }
    }
    
    func setupView() {
        let dateFormatted = item?.createdAt?.formatDateDefault()
        setUserInformation(userName: item?.user?.login, userFullName: dateFormatted, avatarUrl: item?.user?.avatarUrl)
        setRepositoryInformatio(title: item?.title, detail: item?.body)
    }
}
