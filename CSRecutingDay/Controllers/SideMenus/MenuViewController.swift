//
//  MenuViewController.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 03/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit
import SDWebImage

class MenuViewController: UIViewController {
    //TODO: Colocar em um arquivo a parte
    let userImageURL = "https://media.licdn.com/mpr/mpr/shrinknp_400_400/p/5/005/045/190/2b451b2.jpg"
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var developerImage: UIImageView!
    let languageList = ["Java", "Swift", "Javascript", "Objective-C", "Scala", "Kotlin", "Python", "C#", "PHP"]
    var simpleDataSource: SimpleInformationDataSource?
    static let SelectedLanguage = "SelectedLanguage"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
}

extension MenuViewController {
    func setupController() {
        setDeveloperImage()
        simpleDataSource = SimpleInformationDataSource(listItem: languageList, tableView: tableView)
        tableView.dataSource = simpleDataSource
        tableView.reloadData()
    }
    
    func setDeveloperImage() {
        if let imageURL = URL(string: userImageURL) {
            developerImage.sd_setImage(with: imageURL, completed: nil)
        }
    }
}

extension MenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let language = languageList[indexPath.row]
        MenuViewController.SelectedLanguage.postNotification(object: language)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
