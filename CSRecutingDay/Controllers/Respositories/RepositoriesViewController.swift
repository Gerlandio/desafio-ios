//
//  ViewController.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 28/10/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit
import Rswift

class RepositoriesViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var repoDataSource: RepositoriesDataSource?
    
    var currentRepoList = [Repository]()
    let repositorieService = RepositoriesServiceImpl()
    
    fileprivate var currentPage = 1
    fileprivate var currentLanguage = "Java" {
        didSet{
            fetchData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        setupView()
    }
}

extension RepositoriesViewController {
    
    func fetchData() {
        self.loadingState = .loading
        repositorieService.fetchRepositories(language: currentLanguage, page: currentPage) { (result) in
            switch result {
            case .success(let repositories):
                self.loadingState = .ready
                self.currentPage += 1
                self.currentRepoList.append(contentsOf: repositories.items ?? [])
                self.setupController()
            case .error(let message):
                self.loadingState = .error
                print(message)
            }
        }
    }
}

extension RepositoriesViewController {
    
    func setupController() {
        repoDataSource = RepositoriesDataSource(listItem: currentRepoList, tableView: self.tableView)
        self.tableView.dataSource = repoDataSource
        self.tableView.reloadData()
    }
    
    func setupView() {
        setupNavigation()
        MenuViewController.SelectedLanguage.registerNotification(listener: self, callback: #selector(setupLanguage(notification:)))
    }
    
    func setupNavigation() {
        currentRepoList = []
        loadingState = .initial
        self.navigationItem.title = "GitHub \(currentLanguage)"
    }
    
    @objc func setupLanguage(notification: Notification) {
        if let language = notification.object as? String {
            currentLanguage = language
            setupNavigation()
        }
    }
}

extension RepositoriesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        //Garantir nunca é demais
        if  self.currentRepoList.count > indexPath.row {
           let repository = self.currentRepoList[indexPath.row]
            PullRequestPresenter.showPullRequestViewController(repository: repository, from: self)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastRowNumber = self.currentRepoList.count - 1
        if indexPath.row ==  lastRowNumber {
            fetchData()
        }
    }
}
