//
//  BaseViewController.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 02/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    var initialView: LoadingFeedbackView?
    
    enum LoadingState {
        case initial
        case loading
        case ready
        case error
    }
    
    var loadingState: LoadingState = .initial {
        didSet {
            switch loadingState {
            case .initial:
                self.initialView?.isHidden = false
            case .loading:
                self.activityIndicator?.startAnimating()
            case .ready:
                self.activityIndicator?.stopAnimating()
                self.initialView?.isHidden = true
            case .error:
                self.activityIndicator?.stopAnimating()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialView = LoadingFeedbackView(parentView: self.view)
    }
}
