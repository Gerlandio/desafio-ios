//
//  PullRequestsViewController.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 02/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit

class PullRequestsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var openPullCount: UILabel!
    @IBOutlet weak var closedPullCount: UILabel!
    
    var currentPullList:[PullRequest] = []
    let pullRequestsImpl = PullRequestsServiceImpl()
    var currentRepositorie: Repository?
    var pullDataSource: PullRequestDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
    }
}

extension PullRequestsViewController {
    func fetchData() {
        self.loadingState = .loading
        pullRequestsImpl.fetchPullRequests(fullName: currentRepositorie?.fullName ?? "") { (result) in
            switch result {
            case .success(let pullRequests):
                self.loadingState = .ready
                self.currentPullList.append(contentsOf: pullRequests)
                self.setupController()
            case .error(_):
                self.loadingState = .error
            }
        }
    }
}

extension PullRequestsViewController {
    
    func setupController() {
        setupView()
        pullDataSource = PullRequestDataSource(listItem: currentPullList, tableView: self.tableView)
        self.tableView.dataSource = pullDataSource
        self.tableView.reloadData()
    }
    
    func setupView() {
        self.openPullCount.text = "\(currentPullList.filter({ $0.state == "open"}).count) opened"
        self.closedPullCount.text = "/\(currentPullList.filter({ $0.state == "close"}).count) closed"
        self.navigationItem.title = self.currentRepositorie?.name
    }
}

extension PullRequestsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if  self.currentPullList.count > indexPath.row {
            let pullRequest = self.currentPullList[indexPath.row]
            if let htmlURL = pullRequest.htmlUrl, let urlPull = URL(string: htmlURL) {
                PullRequestPresenter.showWebViewController(urlPullRequest: urlPull, from: self)
            }
        }
    }
}
