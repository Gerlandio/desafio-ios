//
//  WebViewController.swift
//  CSRecrutingDay
//
//  Created by Gerlandio Da Silva Lucena on 04/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit

class WebViewController: BaseViewController {
    
    @IBOutlet weak var webView: UIWebView!
    var url: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
}

extension WebViewController {
    
    func setupController() {
        if let urlPullRequest = url {
            let request = URLRequest(url: urlPullRequest)
            webView.loadRequest(request)
        }
    }
    
    func setupTitle(title: String) {
        self.navigationItem.title = title
    }
}

extension WebViewController: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        loadingState = .loading
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loadingState = .ready
        if let title = webView.stringByEvaluatingJavaScript(from: "document.title") {
            setupTitle(title: title)
        }
    }
}
