//
//  LoadingFeedbackView.swift
//  CSRecutingDay
//
//  Created by Gerlandio Da Silva Lucena on 04/11/17.
//  Copyright © 2017 GerlandioLucena. All rights reserved.
//

import UIKit
import SnapKit

class LoadingFeedbackView: UIView {
    
    lazy var activityIndicator: UIActivityIndicatorView  = {
        let activity = UIActivityIndicatorView()
        activity.color = UIColor.defaultOrangeColor
        activity.hidesWhenStopped = false
        activity.startAnimating()
        return activity
    }()
    
    lazy var labelDescription: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15.0)
        label.textAlignment = .center
        return label
    }()

    init(parentView: UIView, feedbackMessage: String = "Carregando...") {
        super.init(frame: CGRect.zero)
        parentView.addSubview(self)
        labelDescription.text = feedbackMessage
        setupCurrentView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Não implementado para esta classe.")
    }
    
    func setupCurrentView() {
        self.backgroundColor = UIColor.white
        self.addSubview(activityIndicator)
        self.addSubview(labelDescription)
        
        activityIndicator.snp.makeConstraints { (maker) in
            maker.centerX.equalToSuperview()
            maker.centerY.equalToSuperview()
        }
        
        labelDescription.snp.makeConstraints { (maker) in
            maker.leftMargin.equalToSuperview().offset(12.0)
            maker.rightMargin.equalToSuperview().offset(12.0)
            maker.topMargin.equalTo(activityIndicator.snp.bottom).offset(12.0)
        }
        
        self.snp.makeConstraints { (maker) in
            if let superview = superview {
                maker.left.equalTo(superview.snp.left)
                maker.right.equalTo(superview.snp.right)
                maker.bottom.equalTo(superview.snp.bottom)
                maker.top.equalTo(superview.snp.top)
            }
        }
    }
}
